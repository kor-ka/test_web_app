package io.korinc.test_web_view;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText et;
    private WebView wv;
    private CheckBox chb;
    private ProgressBar pb;
    private KeyboardHelper mHelper;

    private LogsFragment logsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et = (EditText) findViewById(R.id.et);
        wv = (WebView) findViewById(R.id.webview);
        chb = (CheckBox) findViewById(R.id.chb);
        pb = (ProgressBar) findViewById(R.id.pb);

        mHelper = new KeyboardHelper(this);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        logsFragment = new LogsFragment();
        transaction.replace(R.id.left_drawer, logsFragment);

        transaction.commit();


        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    if(et.getText().length() > 0){
                        pb.setProgress(0);
                        pb.setIndeterminate(true);
                        String url = et.getText().toString();
                        if(!url.startsWith("http")){
                            url = "http://" + url;
                        }
                        et.setText(url);
                        et.setSelection(url.length());
                        mHelper.setImeVisibility(et, false);
                        wv.loadUrl(url);
                    }
                    return true;
                }
                return false;
            }
        });

        chb.setChecked(false);

        chb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                wv.getSettings().setJavaScriptEnabled(b);
                wv.reload();
            }
        });


        wv.setWebChromeClient(new WebChromeClient(){
            @Override
            public boolean onConsoleMessage(ConsoleMessage cm) {
                Log.d("WEBVIEW_LOG", String.format("%s @ %d: %s",
                        cm.message(), cm.lineNumber(), cm.sourceId()));
                logsFragment.onEntry(cm);
                return true;
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                pb.setIndeterminate(false);
                pb.setProgress(newProgress);
            }

        });

       wv.setWebViewClient(new WebViewClient());

    }
}
