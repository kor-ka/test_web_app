package io.korinc.test_web_view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by gputintsev on 24.07.17.
 */

public class LogsFragment extends Fragment {

    private RecyclerView list;

    private CMAdapter mAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        if(list == null){

        }

        list = new RecyclerView(getContext());

        list.setBackgroundColor(0xffffffff);

        LinearLayoutManager layout = new LinearLayoutManager(getContext());
        layout.setReverseLayout(true);
        list.setLayoutManager(layout);

        mAdapter = new CMAdapter();

        list.setAdapter(mAdapter);


        return list;
    }

    public void onEntry(ConsoleMessage cm) {
        mAdapter.add(cm);

    }

    private class  CMAdapter extends RecyclerView.Adapter<CMHolder>{
        ArrayList<ConsoleMessage> logs = new ArrayList<>();

        public void add(ConsoleMessage cm){
            logs.add(0, cm);
            notifyItemInserted(0);
        }

        @Override
        public CMHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new CMHolder(new TextView(getContext()));
        }

        @Override
        public void onBindViewHolder(CMHolder holder, int position) {
            holder.bind(logs.get(position));
        }

        @Override
        public int getItemCount() {
            return logs.size();
        }
    }

    private class CMHolder extends RecyclerView.ViewHolder{

        TextView enty;
        public CMHolder(View itemView) {
            super(itemView);
            enty = (TextView) itemView;
        }

        public void bind(ConsoleMessage cm){
            enty.setText( String.format("%s @ %d: %s",
                    cm.message(), cm.lineNumber(), cm.sourceId()));
        }
    }

}
